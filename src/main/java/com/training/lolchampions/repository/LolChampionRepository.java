package com.training.lolchampions.repository;

import com.training.lolchampions.entity.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository<LolChampion, Long> {

}
